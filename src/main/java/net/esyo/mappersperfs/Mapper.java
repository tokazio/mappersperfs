package net.esyo.mappersperfs;

/**
 *
 * @author rpetit
 * @param <S> Source type
 * @param <D> Dest type
 */
public interface Mapper<S,D> {
    
    D to(S source,Class<D> destClass);
    
}
