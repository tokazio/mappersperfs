package net.esyo.mappersperfs.dozer;

import java.io.File;
import java.util.Arrays;
import net.esyo.mappersperfs.Mapper;
import org.dozer.DozerBeanMapper;

/**
 * @see http://dozer.sourceforge.net/
 * @author rpetit
 */
public class DozerMapper implements Mapper {

    org.dozer.Mapper mapper;

    public DozerMapper() {
        mapper = new DozerBeanMapper();
        configureMapper(new File("src/main/resources/dozer/mapping.xml").toURI().toString());
    }
    
    private void configureMapper(String... mappingFileUrls) {
        ((DozerBeanMapper) mapper).setMappingFiles(Arrays.asList(mappingFileUrls));
    }

    @Override
    public Object to(Object source, Class destClass) {
        return mapper.map(source, destClass);
    }

}
