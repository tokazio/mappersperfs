package net.esyo.mappersperfs;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rpetit
 */
public class Person {

    private Name name;
    private List<Name> knownAliases;
    private Date birthDate;

    public Person(Name name, Date birthDate,
            List<Name> knownAliases) {
        this.name = name;
        this.birthDate = new Date(birthDate.getTime());
        this.knownAliases = new ArrayList<>(knownAliases);
    }

    public List<Name> getKnownAliases() {
        return Collections.unmodifiableList(knownAliases);
    }

    public Name getName() {
        return name;
    }

    public Date getBirthDate() {
        return new Date(birthDate.getTime());
    }

    public void setName(Name name) {
        this.name = name;
    }

    public void setKnownAliases(List<Name> knownAliases) {
        this.knownAliases = knownAliases;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", knownAliases=" + knownAliases + ", birthDate=" + birthDate + '}';
    }
    
    
}
