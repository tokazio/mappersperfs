package net.esyo.mappersperfs.selma;

import net.esyo.mappersperfs.PersonDto;
import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.Name;
import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;

/**
 *
 * @author rpetit
 */
@Mapper(
    withCustomFields = {
        @Field({"firstName","name.first"}),
        @Field({"lastName","name.last"}),
        @Field({"aliasesList","knownAliases"})
    },
    withIgnoreMissing = IgnoreMissing.ALL,
    withImmutables = Name.class
)
public interface PersonMapper {

    PersonDto asPersonDTO(Person source);
}
