package net.esyo.mappersperfs.selma;

import net.esyo.mappersperfs.Person;
import fr.xebia.extras.selma.Selma;
import net.esyo.mappersperfs.Mapper;

/**
 * @see http://www.selma-java.org/
 * @author rpetit
 */
public class SelmaMapper<S,D> implements Mapper<S,D> {
    
    private final PersonMapper mapper;
    
    public SelmaMapper(){
        mapper = Selma.builder(PersonMapper.class).build();
    }
    
    @Override
    public D to(S source,Class<D> destClass){
        Person s = (Person) source;
        return (D) mapper.asPersonDTO(s);
    }

}
