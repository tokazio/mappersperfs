package net.esyo.mappersperfs.mapstruct;

import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.PersonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author rpetit
 */
@Mapper(componentModel = "spring")
public interface PersonMapper {
    
    PersonMapper INSTANCE = Mappers.getMapper( PersonMapper.class );
    
    @Mappings({
        @Mapping(source = "name.first", target = "firstName"),
        @Mapping(source = "name.last", target = "lastName"),
        @Mapping(source = "knownAliases", target = "aliasesList")
    })
    PersonDto toDto(Person person);
    
}
