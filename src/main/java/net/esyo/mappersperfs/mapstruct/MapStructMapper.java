package net.esyo.mappersperfs.mapstruct;

import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.Mapper;

/**
 * @see https://github.com/mapstruct/mapstruct
 * @author rpetit
 */
public class MapStructMapper<S,D> implements Mapper<S,D> {
    
    @Override
    public D to(S source,Class<D> destClass){
        Person s = (Person) source;
        return (D) PersonMapper.INSTANCE.toDto(s);
    }
    
    

}
