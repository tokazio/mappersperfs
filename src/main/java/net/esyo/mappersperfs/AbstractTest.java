package net.esyo.mappersperfs;

/**
 *
 * @author rpetit
 */
public abstract class AbstractTest<S, D> {

    private static final int NB = 1000000;

    public abstract Mapper getMapper();

    public abstract S getSource();

    public abstract Class<D> getDestClass();

    public abstract String getName();
    
    public D test(){
        return (D) getMapper().to(getSource(), getDestClass());
    }
    
    public double run(){
        System.out.println(getName() + "============================================");
        //warmup
        for (int i = 0; i < 100; i++) {
            test();
        }
        //test
        long s = System.nanoTime();
        for (int i = 0; i < NB; i++) {
            test();
        }
        long e = System.nanoTime();
        double t = (e - s) * 1.0e-6;
        System.out.println(NB + " in " + t + "ms -> " + (NB / t) + " mapping/sec");
        return (NB / t);
    }

}
