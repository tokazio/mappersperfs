package net.esyo.mappersperfs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import net.esyo.mappersperfs.dozer.DozerTest;
import net.esyo.mappersperfs.dtofactory.DTOFactoryTest;
import net.esyo.mappersperfs.jmapper.JMapperTest;
import net.esyo.mappersperfs.manual.ManualTest;
import net.esyo.mappersperfs.mapstruct.MapStructTest;
import net.esyo.mappersperfs.modelmapper.ModelmapperTest;
import net.esyo.mappersperfs.orika.OrikaTest;
import net.esyo.mappersperfs.selma.SelmaTest;

/**
 *
 * @author rpetit
 */
public class Run {

    private static final int NB = 3;
    private static final Map<String, List<Double>> TEMPS = new TreeMap<>();

    public static void main(String[] args) {
        for (int i = 0; i < NB; i++) {
            System.out.println("=============================================");
            System.out.println("TOUR " + (i + 1) + "/" + NB);
            System.out.println("=============================================");

            try {
                add("Manual", new ManualTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("MapStruct", new MapStructTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("JMapper", new JMapperTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("Selma", new SelmaTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("Orika", new OrikaTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("Modelmapper", new ModelmapperTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("DTOFactory", new DTOFactoryTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
            try {
                add("Dozer", new DozerTest().run());
            } catch (Exception ex) {
                System.out.println("Failed");
                ex.printStackTrace();
            }
        }
        report();
    }

    private static void report() {
        System.out.println("\n=================================================");
        System.out.println("Moyenne(s) sur " + NB + " tour(s)");
        System.out.println("=================================================");
        double maxMoy = 0;
        for (Entry<String, List<Double>> e : TEMPS.entrySet()) {
            double moy = moyenne(e.getValue());
            if (moy > maxMoy) {
                maxMoy = moy;
            }
        }
        Map<String, Double> classement = new LinkedHashMap<>();
        for (Entry<String, List<Double>> e : TEMPS.entrySet()) {
            double moy = moyenne(e.getValue());
            double perc = ((moy / maxMoy) * 100);
            classement.put(e.getKey(), perc);
            System.out.println(e.getKey() + "=" + moy + " mapping/sec (" + perc + "%)");
        }
        classement = sortByValue(classement);

        System.out.println("=================================================");
        for (Entry<String, Double> e : classement.entrySet()) {
            System.out.println(space(e.getKey()) + ": [" + barre(e.getValue())+"] "+Math.round(e.getValue())+"%");
        }
    }

    private static String space(String txt){
        StringBuilder sb = new StringBuilder(txt);
        while(sb.toString().length()<12){
            sb.append(" ");
        }
        return sb.toString();
    }

    private static String barre(double val){
        int v = (int) val;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<v;i++){
            sb.append("=");//(char)219
        }
        while(sb.toString().length()<100){
            sb.append(" ");//(char)176
        }
        return sb.toString();
    }

    private static double moyenne(List<Double> liste) {
        double tot = 0;
        for (Double d : liste) {
            tot += d;
        }
        return tot / liste.size();
    }

    private static void add(String key, double time) {
        List<Double> liste = TEMPS.get(key);
        if (liste == null) {
            liste = new ArrayList<>(NB);
            TEMPS.put(key, liste);
        }
        liste.add(time);
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list
                = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return -(o1.getValue()).compareTo(o2.getValue());
            }
        });
        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
