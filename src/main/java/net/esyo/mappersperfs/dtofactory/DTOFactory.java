package net.esyo.mappersperfs.dtofactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Copier un objet vers un autre. (java.lang.reflect)
 * Copie des champs de la source vers la destination.
 * 1-Repère les champs <code>private</code>, <code>protected</code>, <code>package</code>, <code>public</code> de la
 * destination (les champs <code>static</code> ne sont pas copiés).
 * 2-Les cherchent dans la source
 * 3-Les copient (par référence) si il sont présent dans la source.
 * Copie directe des champs sans passer par les getters/setters.
 * @From permet de mapper un champs de la source précis, notament un champs d'un sous objet
 * <code>@From("sousobjet.champ");</code>
 * Pourrais être fait:
 * _ préciser par annotation si on passe par le getter et/ou le setter
 * _ préciser par annotation si copie plutôt que référence (== faux)
 * Créé le 24 mars 2017.
 * @author rpetit
 * deprecated devrait être remplacé par l'utilisation de Dozer ou autre framework de mapping
 * @link https://agora.groupe.pharmagest.com/confluence/x/_i47Aw
 */
public class DTOFactory {
    
    private DTOFactory() {
        //hide
    }
    
    /**
     * Crée une instance de <code>classe</code>, copie les champs de l'objet source, retourne cette instance
     */
    public static <T, E> E to(final T fromObj, final Class<E> classe) {
        E toObj;
        try {
            toObj = classe.newInstance();
            copy(fromObj, toObj);
            return toObj;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("DTOFactory::to-> Erreur lors de la création d'un objet destination de type '"
                    + classe.getName()
                    + "', il doit avoir un constructeur vide accessible: une classe et un constructeur vide 'public'!",
                    ex);
        }
    }
    
    /**
     * Copie les champs en communs (non <code>static</code>) de l'objet <code>fromObj</code> vers l'objet
     * <code>toObj</code>.<br>
     * Champs <code>private</code> et <code>protected</code> compris.
     */
    public static <T, E> E copy(final T fromObj, final E toObj) {
        if (fromObj == null) {
            throw new RuntimeException("DTOFactory::copy-> Objet 'from' ne peut être null");
        }
        if (toObj == null) {
            throw new RuntimeException("DTOFactory::copy-> Objet 'to' ne peut être null");
        }
        for (Field f : getClassFields(toObj.getClass(), Object.class)) {
            
                copyField(fromObj, toObj, f);
            
        }
        return toObj;
    }
    
    private static <T, E> E copyField(final T fromObj, final E toObj, final Field f) {
        Source<T> source = new Source<>(fromObj, f);
        //Pas de source
        if (source.object() == null) {
            System.out.println("DTOFactory::copyField-> L'objet source '" + source.fieldName() + "' est null");
            return toObj;
        }
        //Copie Set
        if (Set.class.isAssignableFrom(f.getType())) {
            copySet(source, toObj, f);
            return toObj;
        }
        ////Copie List
        if (List.class.isAssignableFrom(f.getType())) {
            copyList(source, toObj, f);
            return toObj;
        }
        //Copy Field
        try {
            copyField(source, toObj, f);
        } catch (NoSuchFieldException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException | IllegalArgumentException ex) {
            System.out.println("DTOFactory::copyField-> Erreur de copie directe de '" + source.fieldName()
                    + "' vers le champ '" + f.getName()
                    + "', essai avec un nouvel objet '"
                    + f.getType() + "'");
            copyNew(source, toObj, f);
        }
        return toObj;
    }
    
    @SuppressWarnings("unchecked")
    private static <E> Class<E> getFieldType(final Field f) {
        return ((Class<E>) ((ParameterizedType) f.getGenericType()).getActualTypeArguments()[0]);
    }
    
    private static boolean hasAccessibleEmptyConstructor(final Field f) {
        try {
            getFieldType(f).getConstructor();
            return true;
        } catch (NoSuchMethodException | SecurityException ex) {
            System.out.println(
                    "DTOFactory::copyList-> Impossible de copier '" + f.getName()
                            + "' l'object cible ne possèdent pas de constructeur accessible.");
            
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    private static <T, E> void copyList(final Source<T> source, final E toObj, final Field f) {
        try {
            List<Object> liste = (List<Object>) getFieldValue(source.fieldName(), source.object());
            //si l'objet cible ne peut être construit, on copie la référence
            if (!hasAccessibleEmptyConstructor(f)) {
                f.set(toObj, liste);
            } else {
                List<Object> nouvelle = null;
                if (liste != null) {
                    nouvelle = new ArrayList<>();
                    for (Object o : liste) {
                        nouvelle.add(DTOFactory.to(o, getFieldType(f)));
                    }
                }
                f.set(toObj, nouvelle);
            }
        } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException ex) {
            System.out.println(
                    "DTOFactory::copyField-> Erreur lors de la copie de la liste '" + f.getName() + "' dans '"
                            + toObj.getClass() + "' vers '" + f.getType().getName());
        }
    }
    
    @SuppressWarnings("unchecked")
    private static <T, E> void copySet(final Source<T> source, final E toObj, final Field f) {
        try {
            Set<Object> set = (Set<Object>) getFieldValue(source.fieldName(), source.object());
            //si l'objet cible ne peut être construit, on copie la référence
            if (!hasAccessibleEmptyConstructor(f)) {
                f.set(toObj, set);
            } else {
                Set<Object> nouvelle = null;
                if (set != null) {
                    nouvelle = new HashSet<>();
                    for (Object o : set) {
                        nouvelle.add(DTOFactory.to(o, (Class<E>) ((ParameterizedType) f.getGenericType())
                                .getActualTypeArguments()[0]));
                    }
                }
                f.set(toObj, nouvelle);
            }
        } catch (IllegalArgumentException | NoSuchFieldException | IllegalAccessException ex) {
            System.out.println("DTOFactory::copyField-> Erreur d'écriture du champ Set '" + f.getName() + "' dans '"
                    + toObj.getClass() + "', cast vers '" + f.getType().getName());
        }
    }
    
    private static <T, E> void copyField(final Source<T> source, final E toObj, final Field f)
            throws IllegalArgumentException,
            IllegalAccessException,
            NoSuchFieldException {
        Object o = getFieldValue(source.fieldName(), source.object());
        f.set(toObj, o);
    }
    
    private static <T, E> void copyNew(final Source<T> source, final E toObj, final Field f) {
        Object o = null;
        try {
            o = getFieldValue(f.getName(), source.object());
        } catch (IllegalArgumentException | NoSuchFieldException ex) {
            System.out.println("DTOFactory::copyNew-> L'objet '" + source.object().getClass().getSimpleName() + "."
                    + f.getName() + "' source est null");
            return;
        }
        if (o != null) {
            
            System.out.println("DTOFactory::copyNew-> Copie " + source.object().getClass() + "." + f.getName() + "(du type "
                    + o.getClass().getSimpleName() + ") vers " + toObj.getClass() + "." + f.getName() + " (du type "
                    + f.getType().getSimpleName() + ") via le constructeur " + f.getType() + "("
                    + o.getClass().getSimpleName() + ")...");
            Constructor<?> c = null;
            try {
                c = f.getType().getConstructor(o.getClass());
            } catch (NoSuchMethodException | SecurityException e) {
                System.out.println(
                        "DTOFactory::copyNew-> Pas de constructeur '" + f.getType().getSimpleName() + "<init>("
                                + o.getClass().getSimpleName()
                                + ") pour copier '" + o.getClass().getSimpleName() + "' vers '"
                                + f.getType().getSimpleName() + "'... recherche d'un constructeur avec <init>(Oject)");
                try {
                    c = f.getType().getConstructor(Object.class);
                } catch (NoSuchMethodException | SecurityException ex) {
                    System.out.println(
                            "DTOFactory::copyNew-> Pas de constructeur '" + f.getType().getSimpleName() + "<init>("
                                    + o.getClass().getSimpleName()
                                    + ") pour copier '" + o.getClass().getSimpleName() + "' vers '"
                                    + f.getType().getSimpleName() + "'");
                }
            }
            if (c != null) {
                try {
                    f.set(toObj, c.newInstance(o));
                    System.out.println("DTOFactory::copyNew-> copié: " + f.getName() + " (" + f.getType().getName()
                            + ") - source: "
                            + (source.from() != null ? " @From(" + source.from().value() + ")"
                                    : source.fieldName()));
                } catch (IllegalArgumentException | IllegalAccessException | InstantiationException
                        | InvocationTargetException ex) {
                    System.out.println(
                            "DTOFactory::copyNew-> Impossible de copier (Field.set) " + source.object().getClass() + "."
                                    + f.getName()
                                    + "(du type "
                                    + o.getClass().getSimpleName() + ") vers " + toObj.getClass() + "." + f.getName()
                                    + " (du type "
                                    + f.getType().getSimpleName() + ") via le constructeur " + f.getType() + "("
                                    + o.getClass().getSimpleName() + ")");
                }
                
            }
        }
    }
    
    /**
     * Recherche du champ (non <code>static</code>) <code>fieldName</code> dans la classe <code>clazz</code> jusqu'à la
     * super classe
     * <code>superClazz</code>.<br>
     * Champs <code>private</code>, <code>package</code> et <code>protected</code> compris.
     */
    public static Field getClassField(final String fieldName, final Class<?> clazz, final Class<?> superClazz) {
        for (Field f : getClassFields(clazz, superClazz)) {
            if (f.getName().equals(fieldName)) {
                f.setAccessible(true);
                return f;
            }
        }
        return null;
    }
    
    /**
     * Recherche de la méthode <code>methodName</code> avec les paramètres de type <code>paramTypes</code> dans
     * la classe <code>clazz</code> jusqu'à la super classe <code>superClazz</code>.<br>
     * Méthodes <code>private</code>, <code>package</code> et <code>protected</code> comprises.
     */
    public static Method getClassMethod(final String methodName, final Class<?> clazz, final Class<?> superClazz,
            final Class<?>... paramTypes) {
        for (Method m : getClassMethods(clazz, superClazz)) {
            if (m.getName().equals(methodName)) {
                m.setAccessible(true);
                for (int i = 0; i < m.getParameterTypes().length; i++) {
                    if (m.getParameterTypes()[i] != paramTypes[i]) {
                        return null;
                    }
                }
                return m;
            }
        }
        return null;
    }
    
    /**
     * Liste des champs (non <code>static</code>) de la classe <code>clazz</code>, y compris ceux jusqu'à la super
     * classe <code>superClazz</code>.<br>
     * Champs <code>private</code>, <code>package</code> et <code>protected</code> compris.
     */
    public static Set<Field> getClassFields(final Class<?> clazz, final Class<?> superClazz) {
        Set<Field> liste = new HashSet<>();
        if (clazz != null) {
            Class<?> cl = clazz;
            do {
                for (Field f : cl.getDeclaredFields()) {
                    if (!Modifier.isStatic(f.getModifiers())) {
                        liste.add(f);
                        f.setAccessible(true);
                    }
                }
                cl = cl.getSuperclass();
            } while (clazz != superClazz && cl != superClazz);
        }
        return liste;
    }
    
    /**
     * Liste des methodes (non <code>static</code> de la classe <code>clazz</code>, y compris ceux jusqu'à la super
     * classe <code>superClass</code>.<br>
     * Méthodes <code>private</code>, <code>package</code> et <code>protected</code> comprises.
     */
    public static Set<Method> getClassMethods(final Class<?> clazz, final Class<?> superClazz) {
        Set<Method> liste = new HashSet<>();
        if (clazz != null) {
            Class<?> cl = clazz;
            do {
                for (Method m : cl.getDeclaredMethods()) {
                    if (!Modifier.isStatic(m.getModifiers())) {
                        m.setAccessible(true);
                        liste.add(m);
                    }
                }
                cl = cl.getSuperclass();
            } while (clazz != superClazz && cl != superClazz);
        }
        return liste;
    }
    
    /**
     * Recherche la valeur du champ (non <code>static</code>) <code>fieldName</code> dans la classe <code>clazz</code>
     * de l'objet <code>object</code> (jusqu'à la super classe la plus haute).<br>
     * Champs <code>private</code>, <code>package</code> et <code>protected</code> compris.
     */
    public static <T> Object getFieldValue(final String fieldName, final T object)
            throws IllegalArgumentException,
            NoSuchFieldException {
        if (object == null) {
            throw new NoSuchFieldException(
                    "DTOFactory::getFieldValue-> Impossible de trouver le champ '" + fieldName
                            + "' dans un objet null");
        }
        if (fieldName == null || fieldName.isEmpty()) {
            throw new NoSuchFieldException(
                    "DTOFactory::getFieldValue-> Le champ ne peut être ni null ni vide dans '" + object.getClass()
                            + "'");
        }
        Field f = getClassField(fieldName, object.getClass(), null);
        if (f != null) {
            try {
                return f.get(object);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
                
            } catch (Exception lzex) {
                System.out.println(
                        "DTOFactory::getFieldValue-> Exception (LazyInitializationException?) à la récupération de '"
                                + f.getClass().getName() + "."
                                + f.getName() + "'");
            }
        }
        throw new NoSuchFieldException(
                "DTOFactory::getFieldValue-> Champ '" + fieldName + "' introuvable dans '" + object.getClass() + "'");
    }
    
    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------
    private static class Source<T> {
        
        private Object sourceObj;
        private String sourceFieldName;
        private final From from;
        
        Source(final T fromObj, final Field f)
                throws IllegalArgumentException {
            sourceFieldName = f.getName();
            sourceObj = fromObj;
            from = f.getAnnotation(From.class);
            if (from != null) {
                sourceFieldName = from.value();
                if (sourceFieldName.contains(".")) {
                    LinkedList<String> names = new LinkedList<>(Arrays.asList(sourceFieldName.split("\\.")));
                    sourceFieldName = names.pollLast();
                    for (String name : names) {
                        try {
                            sourceObj = DTOFactory.getFieldValue(name, sourceObj);
                        } catch (IllegalArgumentException | NoSuchFieldException ex) {
                            throw new IllegalArgumentException(
                                    "DTOFactory::Source<init>-> Impossible de récupérer le champ source '"
                                            + sourceFieldName + "'",
                                    ex);
                        }
                    }
                }
            }
        }
        
        public Object object() {
            return sourceObj;
        }
        
        public String fieldName() {
            return sourceFieldName;
        }
        
        public From from() {
            return from;
        }
    }
}
