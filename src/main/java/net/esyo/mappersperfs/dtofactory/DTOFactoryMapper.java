package net.esyo.mappersperfs.dtofactory;

import java.util.logging.Level;
import java.util.logging.Logger;
import ma.glasnost.orika.impl.ConfigurableMapper;
import net.esyo.mappersperfs.Mapper;

/**
 * @see https://github.com/orika-mapper/orika
 * @author rpetit
 */
public class DTOFactoryMapper<S,D> extends ConfigurableMapper implements Mapper<S,D> {
    
    @Override
    public D to(S source,Class<D> destClass){
        try {
            D obj = destClass.newInstance();
            return (D) DTOFactory.copy(source, obj);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(DTOFactoryMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
