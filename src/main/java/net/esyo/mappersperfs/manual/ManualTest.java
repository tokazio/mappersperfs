package net.esyo.mappersperfs.manual;

import net.esyo.mappersperfs.PersonDto;
import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.Name;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.esyo.mappersperfs.Mapper;
import net.esyo.mappersperfs.AbstractTest;

/**
 *
 * @author rpetit
 */
public class ManualTest extends AbstractTest<Person,PersonDto>{

    private final Mapper mapper;
    private final List<Name> nicks = new ArrayList<>();
    private final Person source;

    public ManualTest() throws ParseException{
        mapper = new ManualMapper();
        nicks.add(new Name("tokazio","tokazio"));
        source = new Person(new Name("Romain", "PETIT"), new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"), nicks);
    }

    @Override
    public Mapper getMapper() {
        return mapper;
    }

    @Override
    public Person getSource() {
        return source;
    }

    @Override
    public Class<PersonDto> getDestClass() {
        return PersonDto.class;
    }

    @Override
    public String getName() {
        return "Manual";
    }
        
}
