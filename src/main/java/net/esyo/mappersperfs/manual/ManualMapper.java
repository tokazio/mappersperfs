package net.esyo.mappersperfs.manual;

import ma.glasnost.orika.impl.ConfigurableMapper;
import net.esyo.mappersperfs.Mapper;
import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.PersonDto;

/**
 * @see https://github.com/orika-mapper/orika
 * @author rpetit
 */
public class ManualMapper<S,D> extends ConfigurableMapper implements Mapper<S,D> {
    
    @Override
    public D to(S source,Class<D> destClass){
        Person src = (Person) source;
        PersonDto dest = new PersonDto();
        
        dest.setFirstName(src.getName().getFirst());
        dest.setLastName(src.getName().getLast());
        dest.setBirthDate(src.getBirthDate());
        dest.setAliasesList(src.getKnownAliases());
        return (D) dest;
    }

}
