package net.esyo.mappersperfs.orika;

import net.esyo.mappersperfs.PersonDto;
import net.esyo.mappersperfs.Person;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import net.esyo.mappersperfs.Mapper;

/**
 * @see https://github.com/orika-mapper/orika
 * @author rpetit
 */
public class OrikaMapper<S,D> extends ConfigurableMapper implements Mapper<S,D> {
    
    @Override
    public D to(S source,Class<D> destClass){
        return map(source, destClass);
    }
    
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Person.class, PersonDto.class)
                .field("name.first", "firstName")
                .field("name.last", "lastName")
                .field("knownAliases", "aliasesList")
                .byDefault()
                .register();
    }

}
