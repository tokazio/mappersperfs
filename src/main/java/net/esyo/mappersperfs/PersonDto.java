package net.esyo.mappersperfs;



import java.util.Date;
import java.util.List;
import net.esyo.mappersperfs.dtofactory.From;

/**
 *
 * @author rpetit
 */
public class PersonDto {

    @From("name.first")
    private String firstName;
    @From("name.last")
    private String lastName;
    private Date birthDate;
    @From("knownAliases")
    private List<Name> aliasesList;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Name> getAliasesList() {
        return aliasesList;
    }

    public void setAliasesList(List<Name> aliasesList) {
        this.aliasesList = aliasesList;
    }

    @Override
    public String toString() {
        return "PersonDto{" + "firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate + ", aliasesList=" + aliasesList + '}';
    }
    
    
}
