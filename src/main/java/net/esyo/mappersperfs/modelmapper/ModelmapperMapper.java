package net.esyo.mappersperfs.modelmapper;

import net.esyo.mappersperfs.Mapper;
import org.modelmapper.ModelMapper;

/**
 * @see https://github.com/mapstruct/mapstruct
 * @author rpetit
 */
public class ModelmapperMapper<S, D> implements Mapper<S, D> {

    ModelMapper mapper;

    public ModelmapperMapper() {
        mapper = new ModelMapper();
    }

    @Override
    public Object to(Object source, Class destClass) {
        return mapper.map(source, destClass);
    }

}
