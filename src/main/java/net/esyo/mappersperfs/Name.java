package net.esyo.mappersperfs;

/**
 *
 * @author rpetit
 */
public class Name {

    private String first;
    private String last;

    public Name(){
        //needed by Dozer
    }
    
    public Name(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setLast(String last) {
        this.last = last;
    }    

    @Override
    public String toString() {
        return "Name{" + "first=" + first + ", last=" + last + '}';
    }
    
    
}
