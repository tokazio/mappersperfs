package net.esyo.mappersperfs.jmapper;

import com.googlecode.jmapper.JMapper;
import java.io.File;
import net.esyo.mappersperfs.Mapper;

/**
 * @see https://github.com/jmapper-framework/jmapper-core
 * @author rpetit
 */
public class JMapperMapper<T,E> implements Mapper<T,E> {

    private JMapper<E,T> mapper;
    
    public JMapperMapper(Class<T> sourceClass, Class<E> destClass){
        mapper = new JMapper<E,T>(destClass,sourceClass,new File("src/main/resources/jmapper/mapping.xml").toURI().toString());
    }
    
    @Override
    public E to(T source, Class<E> destClass) {
        return mapper.getDestination(source);
    }
    
}
