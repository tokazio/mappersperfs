
import java.text.SimpleDateFormat;
import net.esyo.mappersperfs.AbstractTest;
import net.esyo.mappersperfs.Person;
import net.esyo.mappersperfs.PersonDto;
import net.esyo.mappersperfs.dozer.DozerTest;
import net.esyo.mappersperfs.dtofactory.DTOFactoryTest;
import net.esyo.mappersperfs.jmapper.JMapperTest;
import net.esyo.mappersperfs.manual.ManualTest;
import net.esyo.mappersperfs.mapstruct.MapStructTest;
import net.esyo.mappersperfs.orika.OrikaTest;
import net.esyo.mappersperfs.selma.SelmaTest;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

/**
 *
 * @author rpetit
 */
public class TestsPerfs {

    @Test
    public void testManual() throws Exception {
        //given

        //when
        PersonDto personDto = new ManualTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }
    
    @Test
    public void testDozer() throws Exception {
        //given

        //when
        PersonDto personDto = new DozerTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }

    @Test
    public void testDTOFactory() throws Exception {
        //given

        //when
        PersonDto personDto = new DTOFactoryTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }

    @Test
    public void testJMapper() throws Exception {
        //given

        //when
        PersonDto personDto = new JMapperTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
    }

    @Test
    public void testMapStruct() throws Exception {
        //given
        AbstractTest<Person,PersonDto> test = new MapStructTest();
        
        //when
        PersonDto personDto = (PersonDto) test.test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }

    @Test
    public void testOrika() throws Exception {
        //given

        //when
        PersonDto personDto = new OrikaTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }

    @Test
    public void testSelma() throws Exception {
        //given

        //when
        PersonDto personDto = new SelmaTest().test();

        //then
        System.out.println(personDto);
        assertThat(personDto).isNotNull();
        assertThat(personDto.getAliasesList()).isNotNull();
        assertThat(personDto.getAliasesList()).isNotEmpty();
        assertThat(personDto.getFirstName()).isEqualTo("Romain");
        assertThat(personDto.getLastName()).isEqualTo("PETIT");
        assertThat(personDto.getBirthDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").parse("13/11/1983"));
        assertThat(personDto.getAliasesList().get(0).getFirst()).isEqualTo("tokazio");
        assertThat(personDto.getAliasesList().get(0).getLast()).isEqualTo("tokazio");
    }
}
